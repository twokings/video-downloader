<?php

namespace Bolt\Extension\Bolt\VideoDownloader\Provider;

use Bolt\Extension\Bolt\VideoDownloader\Controller;
use Bolt\Extension\Bolt\VideoDownloader\Video;
use Silex\Application;
use Silex\ServiceProviderInterface;

/**
 * VideoDownloader Service Provider
 *
 * @author Lodewijk Evers <lodewijk@twokings.nl>
 */
class VideoDownloaderServiceProvider implements ServiceProviderInterface
{
    /** @var array */
    private $config;

    /**
     * Constructor.
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @inheritDoc
     */
    public function register(Application $app)
    {
        $app['videodownloader.config'] = $app->share(
            function () {
                return $this->config;
            }
        );

        $app['videodownloader.video.data'] = $app->share(
            function ($app) {
                return new Video\Data($app);
            }
        );

        $app['videodownloader.controller.backend'] = $app->share(
            function () {
                return new Controller\Backend();
            }
        );

        $app['videodownloader.video.vimeo'] = $app->share(
            function ($app) {
                return new Video\Vimeo($this->config, $app);
            }
        );

        $app['videodownloader.video.youtube'] = $app->share(
            function ($app) {
                return new Video\Youtube($this->config, $app);
            }
        );
    }

    /**
     * @inheritDoc
     */
    public function boot(Application $app)
    {
    }
}
