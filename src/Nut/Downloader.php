<?php

namespace Bolt\Extension\Bolt\VideoDownloader\Nut;

use Bolt\Nut\BaseCommand;
use Silex\Application;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Helper\TableCell;


use Bolt\Extension\Bolt\VideoDownloader\Video;

/**
 * An nut command for the VideoDownloader extension.
 * Use this as $ php app/nut videodownloader:download [--summary] [contenttype]
 *
 * @author Lodewijk Evers <lodewijk@twokings.nl>
 */
class Downloader extends BaseCommand
{

    protected function configure()
    {
        $this
            ->setName('videodownloader:download')
            ->setDescription('Download all new videos from the configured or given contenttype entries')
            ->addArgument(
                'contenttype',
                InputArgument::OPTIONAL,
                'What contenttype is the source'
            )
            ->addOption(
               'summary',
               null,
               InputOption::VALUE_NONE,
               'Display final summary output.'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $contenttype = $input->getArgument('contenttype');

        $starttime = time();
        //$config = $this->app['videodownloader.config'];
        //dump($config);

        $contenttypes = $this->app['videodownloader.video.data']->enabledTypes();
        //dump($contenttypes);

        $downloaderoverview = $this->app['videodownloader.video.data']->getOverviewData();
        //dump($downloaderoverview);

        if ($input->getOption('summary')) {
            $this->downloaderoverview($downloaderoverview, $output);
        }

        if(!empty($contenttype)) {
            // check if the content type exists
            $output->writeln("<info>Downloader starting with " . $contenttype . ".</info> " . date('c', $starttime));
            // check if the content type is configured
            if(!array_key_exists($contenttype, $contenttypes)) {
                $output->writeln("<error>" . $contenttype . " is not configured.</error>");
                return false;
            }
            //else {
            //    $output->writeln("<info>" . $contenttype . " is configured.</info>");
            //}

            // check if the content type is enabled
            if(!(array_key_exists('enabled',  $contenttypes[$contenttype]) && $contenttypes[$contenttype]['enabled']==true)) {
                $output->writeln("<error>" . $contenttype . " is not enabled for import.</error>");
                return false;
            }
            //else {
            //    $output->writeln("<info>" . $contenttype . " is enabled for import.</info>");
            //}
        } else {
            $currenttype = current($contenttypes);
            $contenttype = $currenttype['contenttype'];
            $output->writeln("<info>Downloader starting with default '$contenttype' from configuration.</info> " . date('c', $starttime));
        }

        // download new video's from the configured content type field
        $fieldtoread = $contenttypes[$contenttype]['field'];
        //$output->writeln("<info>" . $contenttype . " has the following field: '$fieldtoread' to read.</info>");

        // set status in the following field
        $statusfield = $contenttypes[$contenttype]['statusfield'];
        //$output->writeln("<info>" . $contenttype . " has the following statusfield: '$statusfield' to read.</info>");

        // Code execution here

        // fetch next batch of videos
        $batch = $this->app['videodownloader.video.data']->getBatch($contenttype);
        $results = [];
        //dump($batch);
        foreach($batch as $video) {
            $output->writeln("<info>Downloading " . $video['title'] . " from ". $video['type'] . ".</info>");
            $currentresult = $this->fetch($video['type'], $video['video_id']);
            //dump($currentresult);
            // set the status to complete
            if($currentresult!==false) {
                $output->writeln("<info>Download complete for " . $video['title'] . " from ". $video['type'] . ".</info>");
                $this->app['videodownloader.video.data']->setDownloadCompleted($contenttype, $video['id'], $currentresult);
                //dump($currentresult);
                $output->writeln("<comment>Downloaded " . $video['title'] . " to ". $currentresult . ".</comment>");
                $results[] = $currentresult;
            } else {
                $output->writeln("<error>Download failed for " . $video['title'] . " from ". $video['type'] . ".</error>");
                $this->app['videodownloader.video.data']->setDownloadFailed($contenttype, $video['id']);
            }
        }

        $num = count($results);
        $endtime = time();
        $output->writeln("<comment>Downloaded $num videos.</comment> ". date('c', $endtime));

        return true;
    }

    /**
     * Show a table with download statuses
     *
     * Yes this duplicates some code from \Nut\Test.php
     *
     * @param OutputInterface $output
     */
    public function downloaderoverview($downloaderoverview, OutputInterface $output) {
        $text = "\n<info>Overview of video downloads</info>\n";
        $output->writeln($text);

        $table = new Table($output);
        $table->setHeaders(
            [
                new TableCell('', array('colspan' => 2)),
                'Total',
                'Processed',
                'Waiting',
                'Invalid'
            ]
        );
        $table->setRows(
            [
                [
                    new TableCell('Videos', array('colspan' => 2)),
                    $downloaderoverview['video']['total'],
                    $downloaderoverview['video']['processed'],
                    $downloaderoverview['video']['waiting'],
                    $downloaderoverview['video']['invalid']
                ],
                new TableSeparator()
            ]
        );
        if(!empty($downloaderoverview['youtube'])) {
            $table->addRows(
                [
                    [
                        '',
                        'Youtube',
                        $downloaderoverview['youtube']['total'],
                        $downloaderoverview['youtube']['processed'],
                        $downloaderoverview['youtube']['waiting'],
                        $downloaderoverview['youtube']['invalid']
                    ]
                ]
            );
        }
        if(!empty($downloaderoverview['vimeo'])) {
            $table->addRows(
                [
                    [
                        '',
                        'Vimeo',
                        $downloaderoverview['vimeo']['total'],
                        $downloaderoverview['vimeo']['processed'],
                        $downloaderoverview['vimeo']['waiting'],
                        $downloaderoverview['vimeo']['invalid']
                    ]
                ]
            );
        }
        if(!empty($downloaderoverview['direct'])) {
            $table->addRows(
                [
                    [
                        '',
                        'Direct',
                        $downloaderoverview['direct']['total'],
                        $downloaderoverview['direct']['processed'],
                        $downloaderoverview['direct']['waiting'],
                        $downloaderoverview['direct']['invalid']
                    ],
                ]
            );
        }
        $table->addRows(
            [
                new TableSeparator(),
                [
                    new TableCell('Other', array('colspan' => 2)),
                    $downloaderoverview['other']['total'],
                    $downloaderoverview['other']['processed'],
                    $downloaderoverview['other']['waiting'],
                    $downloaderoverview['other']['invalid']
                ]
            ]
        );
        $table->render();
        $output->writeln("");
    }

    /**
     * This one will actually fetch one video by type and id
     *
     * example vimeo things
     * $type = 'vimeo';
     * $video_id = '151002965'; // yeasayer i am chemistry
     * $video_id = '128691690'; // voyage of the galactic space dangler
     * $video_id = '151394301'; // love is in the fair
     *
     * example youtube things
     * $type = 'youtube';
     * $video_id = 'MbxVNC_Amag'; // The Daily Show - Birthers Target Ted Cruz
     * $video_id = '7Hv2b_MIj3o'; // Mixmag BAG RAIDERS in The Lab SYD
     * $video_id = 'Gez4_kVIoSo'; // Mixmag AMINE EDGE & DANCE - Boss Ass Bitch [Cuff]
     * $video_id = 'gO3LUhFwx6k'; // California City: The Largest City Never Built | Tom Scott Has a Field Day
     * $video_id = '1sQY3q93EY4'; // Roodgras utrechtTV; Oudegracht bij cafe Weerdzicht.
     *
     * @param $type
     * @param $video_id
     *
     * @return mixed
     */
    public function fetch($type, $video_id)
    {
        if(empty($type) || empty($video_id)) {
            die("Usage:\n\tphp ./index.php -t (vimeo|youtube) -i {some youtube or vimeo id}\n\n");
        }

        $result = false;
        $video = false;

        if($type == 'vimeo') {
            $video = $this->app['videodownloader.video.vimeo'];
        } elseif ($type == 'youtube') {
            $video = $this->app['videodownloader.video.youtube'];
        }

        if($video && !empty($video_id)) {
            $video->setId($video_id);
            // TODO: switch this one on
            $result = $video->download();
        }
        return $result;
    }
}
