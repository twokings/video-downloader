<?php

namespace Bolt\Extension\Bolt\VideoDownloader\Nut;

use Bolt\Nut\BaseCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Helper\TableCell;

use Bolt\Extension\Bolt\VideoDownloader\Video;

/**
 * An nut command for the VideoDownloader extension.
 * Use this as $ php app/nut videodownloader:test
 *
 * @author Lodewijk Evers <lodewijk@twokings.nl>
 */
class Test extends BaseCommand
{
    protected function configure()
    {
        $this
            ->setName('videodownloader:test')
            ->setDescription('Show what is going on')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $text = "\n<info>Overview of video downloads</info>\n";
        $output->writeln($text);

        $downloaderoverview = $this->app['videodownloader.video.data']->getOverviewData();

        $table = new Table($output);
        $table->setHeaders(
                [
                    new TableCell('', array('colspan' => 2)),
                    'Total',
                    'Processed',
                    'Waiting',
                    'Invalid'
                ]
            );
        $table->setRows(
                [
                    [
                        new TableCell('Videos', array('colspan' => 2)),
                        $downloaderoverview['video']['total'],
                        $downloaderoverview['video']['processed'],
                        $downloaderoverview['video']['waiting'],
                        $downloaderoverview['video']['invalid']
                    ],
                    new TableSeparator()
                ]
            );
        if(!empty($downloaderoverview['youtube'])) {
            $table->addRows(
                [
                    [
                        '',
                        'Youtube',
                        $downloaderoverview['youtube']['total'],
                        $downloaderoverview['youtube']['processed'],
                        $downloaderoverview['youtube']['waiting'],
                        $downloaderoverview['youtube']['invalid']
                    ]
                ]
            );
        }
        if(!empty($downloaderoverview['vimeo'])) {
            $table->addRows(
                [
                    [
                        '',
                        'Vimeo',
                        $downloaderoverview['vimeo']['total'],
                        $downloaderoverview['vimeo']['processed'],
                        $downloaderoverview['vimeo']['waiting'],
                        $downloaderoverview['vimeo']['invalid']
                    ]
                ]
            );
        }
        if(!empty($downloaderoverview['direct'])) {
            $table->addRows(
                [
                    [
                        '',
                        'Direct',
                        $downloaderoverview['direct']['total'],
                        $downloaderoverview['direct']['processed'],
                        $downloaderoverview['direct']['waiting'],
                        $downloaderoverview['direct']['invalid']
                    ],
                ]
            );
        }
        $table->addRows(
            [
                new TableSeparator(),
                [
                    new TableCell('Other', array('colspan' => 2)),
                    $downloaderoverview['other']['total'],
                    $downloaderoverview['other']['processed'],
                    $downloaderoverview['other']['waiting'],
                    $downloaderoverview['other']['invalid']
                ]
            ]
        );
        $table->render();
        $output->writeln("");
    }
}
