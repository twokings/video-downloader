<?php

namespace Bolt\Extension\Bolt\VideoDownloader;

use Bolt\Extension\SimpleExtension;
use Bolt\Menu;
use Pimple as Container;
use Silex\Application;

/**
 * VideoDownloader extension loading class.
 *
 * @author Lodewijk Evers <lodewijk@twokings.nl>
 */
class VideoDownloaderExtension extends SimpleExtension
{
    /**
     * {@inheritdoc}
     */
    protected function registerNutCommands(Container $container)
    {
        return [
            new Nut\Downloader($container),
            new Nut\Test($container),
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function getServiceProviders()
    {
        return [
            $this,
            new Provider\VideoDownloaderServiceProvider($this->getConfig()),
        ];
    }

    /**
    * {@inheritdoc}
    */
    protected function registerBackendControllers()
    {
        $app = $this->getContainer();

        return [
            '/extend' => $app['videodownloader.controller.backend'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    protected function registerMenuEntries()
    {
        $menu = (new Menu\MenuEntry('videodownloader', 'videodownloader/overview'))
             ->setLabel('Video Downloader')
             ->setIcon('fa:film')
        ;

        return [$menu];
    }

    /**
     * {@inheritdoc}
     */
    protected function registerTwigFunctions()
    {
        return [
            'getvideodata' => 'getVideoData'
        ];
    }

    /**
     * Lazy way to reuse a the parseVideoUrl in twig
     *
     * @param $video_url
     *
     * @return array
     */
    public function getVideoData($video_url)
    {
        $app = $this->getContainer();
        return $app['videodownloader.video.data']->parseVideoUrl($video_url);
    }

    /**
     * {@inheritdoc}
     */
    protected function registerTwigPaths()
    {
        return ['templates'];
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        return parent::getConfig();
    }

    /**
     * Set the defaults for configuration parameters
     * Should be the same structure as 'config/config.yml.dist'
     *
     * {@inheritdoc}
     */
    protected function getDefaultConfig()
    {
        return [
            'debug' => true,
            'defaults' => [
                'target' => '/var/www/downloads/files',
                'batchlimit'  => 5
            ],
            'import' => [
                'video' => [
                    'enabled' => false,
                    'contenttype' => "your content type here",
                    'downloadfield' => "the field that has the video",
                    'statusfield' => "the field to set if a video has been downloaded"
                ],
            ],
            'providers' => [
                'vimeo' => [
                    'enabled' => true,
                    //'pattern' => '^http(s)?\:\/\/((player|www)\.)?vimeo\.com\/(.*)$',
                    'pattern' => 'vimeo.com',
                    'base' => 'https://player.vimeo.com/video/',
                    'path' => '',
                    'targetpath' => '/vimeo/'
                ],
                'youtube' => [
                    'enabled' => true,
                    //'pattern' => '^http(s)?\:\/\/(www\.)?youtube\.com\/(.*)$',
                    'pattern' => 'youtube.com',
                    'base' => 'https://www.youtube.com/',
                    'path' => 'embed/',
                    'targetpath' => '/youtube/'
                ],
                'direct_url' => [
                    'enabled' => false,
                    //'pattern' => '^http(s)?\:\/\/(www\.)?(.*)\/(.*)$',
                    'pattern' => 'http',
                    'base' => false,
                    'path' => false,
                    'targetpath' => '/direct/'
                ]
            ]
        ];
    }
}
