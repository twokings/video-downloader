<?php

namespace Bolt\Extension\Bolt\VideoDownloader\Controller;

use Bolt\Controller\Backend\BackendBase;
use Bolt\Controller\Zone;
use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Backend controller.
 *
 * @author Lodewijk Evers <lodewijk@twokings.nl>
 */
class Backend extends BackendBase implements ControllerProviderInterface
{

    /**
     * @inheritDoc
     */
    protected function addRoutes(ControllerCollection $c)
    {
        $c->value(Zone::KEY, Zone::BACKEND);

        $c->get('/videodownloader/overview', [$this, 'videoDownloaderOverview'])
            ->bind('videoDownloaderOverview');

        $c->before([$this, 'before']);

        return $c;
    }

    /**
     * Print an overview page with the current status
     *
     * @return Response
     */
    public function videoDownloaderOverview()
    {
        $tv = [];
        $sidebars = [
            'usage' => [
                'icon' => 'info',
                'title' => 'Usage',
                'body' => "The video downloader is invoked from the commandline.
                            You can use it with the following &quot;nut&quot; command
                            which can be added to a cron script.<br>
                            <code>$ php app/nut videodownloader:download</code>",
            ],
            //'warning' => [
            //    'icon' => 'bolt',
            //    'body' => 'testing',
            //]
        ];
        $tg = [
            'content' => 'List the video downloads for all content types that are configured',
            'messages' => [],
            'sidebar' => $sidebars,
        ];

        $tg += $this->app['videodownloader.video.data']->getOverviewData();

        $html = $this->render('videodownloaderinfo.twig', $tv, $tg);

        return new Response(new \Twig_Markup($html, 'UTF-8'));
    }

}
