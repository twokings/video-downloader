<?php
namespace Bolt\Extension\Bolt\VideoDownloader\Video;

use Silex\Application;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Stream;
use Cocur\Slugify\Slugify;
use Dflydev\ApacheMimeTypes;

class Vimeo
{
    var $config;
    var $fullconfig;
    var $url;
    var $id;
    var $title;
    var $app;

    /**
     * Constructor.
     *
     * @param $config
     */
    public function __construct($config, Application $app)
    {
        $this->app = $app;
        $this->fullconfig = $config;
        $this->config = $config['providers']['vimeo'];
    }

    /**
     * Set the vimeo configuration
     */
    public function setConfig($config = array())
    {
        $this->fullconfig = $config;
        $this->config = $config['providers']['vimeo'];
    }

    /**
     * Display the vimeo comfiguration
     */
    public function showConfig()
    {
        print_r($this->config);
    }

    /**
     * Set the vimeo id of avideo
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    public function download()
    {
        $result = $this->getInfoFromContent();

        //dump($result);

        // header('Content-type: text/plain');
        // print "downloader test\n";

        $video = $result['json']->video;
        //dump($video);

        $sources = $result['json']->request->files->progressive;
        //dump($sources);

        $best_id = $this->getOptimalSource($sources);
        $best_source = $sources[$best_id];
        //dump($best_source);
        //dump($best_source->url);

        $target = $this->prepareTargetFileName($video, $best_source);
        //dump($target);

        $saveresult = $this->downloadAndSaveVideoFromURl($best_source->url, $target);

        if($saveresult == $target) {
            return $saveresult;
        }
        return false;
    }

    /**
     * Get a vimeo html document from the base configuration url and a given vimeo video id
     */
    public function getContent()
    {
        //dump($this->config);

        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => $this->config['base'],
            // You can set any number of default request options.
            'timeout'  => 2.0,
        ]);

        $response = $client->request('GET', $this->id);
        $result = $response->getBody();
        $content = $result->getContents();

        // silently create a validated html document
        // TODO: make this work without @
        // TODO: detect missing videos
        $doc = new \DOMDocument();
        if (!@$doc->loadHTML($content)) {
            foreach (libxml_get_errors() as $error) {
                // handle errors here
                print($error);
            }
            libxml_clear_errors();
        }
        //var_dump($doc);

        $validated_doc = $doc->saveHTML();

        return $validated_doc;
    }

    /**
     * Extract the vimeo json object from a vimeo player page and interpret it
     */
    function getInfoFromContent()
    {
        $validated_doc = $this->getContent();
        $output['full'] = $validated_doc;

        // extract the script from the content
        $dom = new \DOMDocument();
        $dom->loadHTML($validated_doc);
        $scripts = $dom->getElementsByTagName('script');
        // ugly hack
        $script = '';
        foreach ($scripts as $tempscript) {
            if(stristr($tempscript->nodeValue, '(function(e,a){var t=')) {
                $script = $tempscript->nodeValue;
            }
        }

        // this assumes the script always looks the same
        // TODO: make sure it knows when the script is broken

        // strip first function declaration
        $script = str_replace('(function(e,a){var t=', '', $script);
        // split the string by the next function
        $script = explode(';', $script);
        // get only the first part
        $output['script'] = $script[0];

        $output['json'] = json_decode($output['script']);

        return $output;
    }

    /**
     * Iterate through the sources and get the highest resolution version
     * Assuming the video has the same aspectratio in all resolutiuons
     */
    public function getOptimalSource($sources)
    {
        $max = 0;
        $best_id = 0;
        foreach($sources as $id => $source) {
            if($max <= $source->width) {
                $max = $source->width;
                $best_id = $id;
            }
        }
        return $best_id;
    }

    /**
     * slugify the title for the filename
     */
    public function prepareTargetFileName($video, $source)
    {
        $slugify = new Slugify();
        $filename = $slugify->slugify($video->title);

        //dump($this->app['paths']);
        //dump($this->config);
        //dump($this->fullconfig);

        $fileextension = $this->getExtension($source->mime);

        $homedir = $this->app['paths']['rootpath'] . $this->fullconfig['defaults']['target'];

        $directory = $homedir . $this->config['targetpath'];

        if(!is_dir($directory)) {
            mkdir($directory);
        }

        $target_name = $directory . $filename . $fileextension;

        // very simple collision prevention
        if(file_exists($target_name)) {
            $timestamp = time();
            $target_name = $directory . $filename . '_' . $timestamp . $fileextension;
        }

        return $target_name;
    }

    /**
     * Download a video and save it to disk
     */
    public function downloadAndSaveVideoFromURl($source_url, $target_file)
    {
        // $original = Stream\create(fopen($source_url, 'r'));
        // $local = Stream\create(fopen($target_file, 'w'));
        // $local->write($original->getContents());

        try {
            //touch($target_file);
            $resource = fopen($target_file, 'w+');
            if (is_writable($target_file)) {
                $client = new Client();
                $request = $client->get($source_url, ['sink' => $resource]);
                //fclose($resource);
                //return $request;
                return $target_file;
            }
        } catch (Exception $e) {
            // Log the error or something
            return false;
            //return $e;
        }

        return null;
    }

    /**
     * Returns file extension of a given mime type
     * @uses Dflydev\ApacheMimeTypes\FlatRepository Mimetype parser library
     * @param  string $mimetype Mime type
     * @return string           File extension of given mime type. it will return "mp4" if no extension could be found
     */
    protected function getExtension($mime_type)
    {
        $mime = new ApacheMimeTypes\FlatRepository;
        $extension = 'mp4';
        $extensions = $mime->findExtensions($mime_type);
        if (count($extensions)) {
            $extension = $extensions[0];
        }
        return '.' . $extension;
    }
}
