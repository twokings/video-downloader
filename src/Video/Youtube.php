<?php
namespace Bolt\Extension\Bolt\VideoDownloader\Video;

use Silex\Application;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Stream;
use Cocur\Slugify\Slugify;
use Dflydev\ApacheMimeTypes;

class Youtube
{
    var $config;
    var $fullconfig;
    var $url;
    var $id;
    var $title;
    var $app;

    /**
     * Constructor.
     *
     * @param $config
     */
    public function __construct($config, Application $app)
    {
        $this->app = $app;
        $this->fullconfig = $config;
        $this->config = $config['providers']['youtube'];
    }

    /**
     * Set the vimeo configuration
     */
    public function setConfig($config = array())
    {
        $this->fullconfig = $config;
        $this->config = $config['providers']['youtube'];
    }

    /**
     * Display the youtube configuration
     */
    public function showConfig()
    {
        print_r($this->config);
    }

    /**
     * Set the youtube id of a video
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    public function download()
    {
        $result = $this->getInfoFromContent();

        $video = $result;
        //dump($video);

        $sources = $result['full_formats'];
        //dump($sources);

        $best_id = $this->getOptimalSource($sources);
        $best_source = $sources[$best_id];
        //dump($best_source);
        //dump($best_source->url);

        $target = $this->prepareTargetFileName($video, $best_source);
        //dump($target);

        $saveresult = $this->downloadAndSaveVideoFromURl($best_source->url, $target);

        if($saveresult == $target) {
            return $saveresult;
        }
        return false;
    }

    /**
     * Get a youtube html document from the base configuration url and a given youtube video id
     */
    public function getContent()
    {
        //dump($this->config);

        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => $this->config['base'],
            // You can set any number of default request options.
            'timeout'  => 2.0,
        ]);

        $response = $client->request('GET', 'get_video_info?el=detailpage&ps=default&eurl=&gl=US&hl=en&sts=15888&video_id=' . $this->id);
        $result = $response->getBody();
        //var_dump($result);
        $content = $result->getContents();
        //var_dump($content);
        parse_str($content, $data);
        //var_dump($data);

        return $data;
    }


    /**
     * Gets informations of Youtube video
     *
     * Stolen from https://github.com/masihyeganeh/YoutubeDownloader
     *
     * @throws Exception If Video ID is wrong or video not exists anymore or it's not viewable anyhow
     *
     * @return object Video's title, images, video length, download links, ...
     */
    function getInfoFromContent()
    {
        $result = array();

        $data = $this->getContent();

        $result['title'] = $data['title'];
        $result['image'] = array(
            'max_resolution' => 'http://i1.ytimg.com/vi/' . $this->id . '/maxresdefault.jpg',
            'high_quality' => 'http://i1.ytimg.com/vi/' . $this->id . '/hqdefault.jpg',
            'medium_quality' => 'http://i1.ytimg.com/vi/' . $this->id . '/mqdefault.jpg',
            'standard' => 'http://i1.ytimg.com/vi/' . $this->id . '/sddefault.jpg',
            'thumbnails' => array(
                'http://i1.ytimg.com/vi/' . $this->id . '/default.jpg',
                'http://i1.ytimg.com/vi/' . $this->id . '/1.jpg',
                'http://i1.ytimg.com/vi/' . $this->id . '/2.jpg',
                'http://i1.ytimg.com/vi/' . $this->id . '/3.jpg'
            )
        );
        $result['length_seconds'] = $data['length_seconds'];

        if (isset($data['ps']) && $data['ps']='live')
        {
            if (!isset($data['hlsvp'])) {
                throw new Exception('This live event is over.', 2);
            }

            $result['stream_url'] = $data['hlsvp'];
        }
        else
        {
            $stream_maps = explode(',', $data['url_encoded_fmt_stream_map']);
            foreach ($stream_maps as $key => $value) {
                parse_str($value, $stream_maps[$key]);

                if (isset($stream_maps[$key]['sig'])) {
                    $stream_maps[$key]['url'] .= '&signature=' . $stream_maps[$key]['sig'];
                    unset($stream_maps[$key]['sig']);
                }

                //$typeParts = explode(';', $stream_maps[$key]['type']);
                $stream_maps[$key] = (object) $stream_maps[$key];
            }
            $result['full_formats'] = $stream_maps;

            if(array_key_exists('adaptive_fmts', $data)) {
                $adaptive_fmts = explode(',', $data['adaptive_fmts']);
                foreach ($adaptive_fmts as $key => $value) {
                    parse_str($value, $adaptive_fmts[$key]);

                    //$typeParts = explode(';', $adaptive_fmts[$key]['type']);
                    $adaptive_fmts[$key] = (object) $adaptive_fmts[$key];
                }
                $result['adaptive_formats'] = $adaptive_fmts;
            }
        }

        return $result;
    }

    /**
     * Iterate through the sources and get the highest resolution version
     *
     * Stolen from https://github.com/pmesco/PHP-YouTube-Download
     */
    public function getOptimalSource($sources)
    {
        $array = array(
            array( 'id' => '264' , 'desc' => 'MP4 (1080p VO)' ),
            array( 'id' => '248' , 'desc' => 'WebM (1080p VOX)' ),
            array( 'id' => '137' , 'desc' => 'MP4 (1080p VO)' ),
            array( 'id' => '85'  , 'desc' => 'MP4 (1080p 3D)' ),
            array( 'id' => '46'  , 'desc' => 'WebM (1080p)' ),
            array( 'id' => '38'  , 'desc' => 'MP4 (1080p)' ),
            array( 'id' => '37'  , 'desc' => 'MP4 (1080p)' ),

            array( 'id' => '247' , 'desc' => 'WebM (720p VOX)' ),
            array( 'id' => '136' , 'desc' => 'MP4 (720p VO)' ),
            array( 'id' => '84'  , 'desc' => 'MP4 (720p 3D)' ),
            array( 'id' => '45'  , 'desc' => 'WebM (720p)' ),
            array( 'id' => '22'  , 'desc' => 'MP4 (720p)' ),

            array( 'id' => '246' , 'desc' => 'WebM (480p VOX)' ),
            array( 'id' => '245' , 'desc' => 'WebM (480p VOX)' ),
            array( 'id' => '244' , 'desc' => 'WebM (480p VOX)' ),
            array( 'id' => '135' , 'desc' => 'MP4 (480p VO)' ),
            array( 'id' => '101' , 'desc' => 'WebM (480p 3D)' ),
            array( 'id' => '83'  , 'desc' => 'MP4 (480p 3D)' ),
            array( 'id' => '44'  , 'desc' => 'WebM (480p)' ),
            array( 'id' => '35'  , 'desc' => 'FLV (480p)' ),

            array( 'id' => '243' , 'desc' => 'WebM (360p VOX)' ),
            array( 'id' => '134' , 'desc' => 'MP4 (360p VO)' ),
            array( 'id' => '100' , 'desc' => 'WebM (360p 3D)' ),
            array( 'id' => '82'  , 'desc' => 'MP4 (360p 3D)' ),
            array( 'id' => '43'  , 'desc' => 'WebM (360p)' ),
            array( 'id' => '34'  , 'desc' => 'FLV (360p)' ),
            array( 'id' => '18'  , 'desc' => 'MP4 (360p)' ),

            array( 'id' => '242' , 'desc' => 'WebM (240p VOX)' ),
            array( 'id' => '133' , 'desc' => 'MP4 (240p VO)' ),
            array( 'id' => '36'  , 'desc' => '3GP (240p)' ),
            array( 'id' => '5'   , 'desc' => 'FLV (240p)' ),

            array( 'id' => '172' , 'desc' => 'WebM (Hi bitrate AO)' ),
            array( 'id' => '141' , 'desc' => 'MP4 (Hi bitrate AO)' ),

            array( 'id' => '171' , 'desc' => 'WebM (Med bitrate AO)' ),
            array( 'id' => '140' , 'desc' => 'MP4 (Med bitrate AO)' ),

            array( 'id' => '139' , 'desc' => 'MP4 (Low bitrate AO)' ),

            array( 'id' => '160' , 'desc' => 'MP4 (144p VO)' ),
            array( 'id' => '17'  , 'desc' => '3GP (144p)' ),
        );

        foreach($sources as $video_id => $source) {
            foreach($array as $encoding) {
                if ($encoding['id'] == $source->itag) {
                    return $video_id;
                }
            }
        }
        return 0; // fallback to first element in array
    }

    /**
     * slugify the title for the filename
     */
    public function prepareTargetFileName($video, $source)
    {
        $slugify = new Slugify();
        $filename = $slugify->slugify($video['title']);

        //dump($this->app['paths']['rootpath']);
        //dump($this->config);
        //dump($this->fullconfig);

        $source->mime = explode(';', $source->type);
        $source->mime = $source->mime[0];
        $fileextension = $this->getExtension($source->mime);

        $homedir = $this->app['paths']['rootpath'] . $this->fullconfig['defaults']['target'];

        $directory = $homedir . $this->config['targetpath'];

        if(!is_dir($directory)) {
            mkdir($directory);
        }

        $target_name = $directory . $filename . $fileextension;

        // very simple collision prevention
        if(file_exists($target_name)) {
            $timestamp = time();
            $target_name = $directory . $filename . '_' . $timestamp . $fileextension;
        }

        return $target_name;
    }

    /**
     * Download a video and save it to disk
     */
    public function downloadAndSaveVideoFromURl($source_url, $target_file)
    {
        // $original = Stream\create(fopen($source_url, 'r'));
        // $local = Stream\create(fopen($target_file, 'w'));
        // $local->write($original->getContents());

        try {
            //touch($target_file);
            $resource = fopen($target_file, 'w+');
            if (is_writable($target_file)) {
                $client = new Client();
                $request = $client->get($source_url, ['sink' => $resource]);
                //fclose($resource);
                //return $request;
                return $target_file;
            }
        } catch (Exception $e) {
            // Log the error or something
            return false;
            //return $e;
        }

        return null;
    }

    /**
     * Returns file extension of a given mime type
     *
     * Stolen from https://github.com/masihyeganeh/YoutubeDownloader
     *
     * @uses Dflydev\ApacheMimeTypes\FlatRepository Mimetype parser library
     * @param  string $mimetype Mime type
     * @return string           File extension of given mime type. it will return "mp4" if no extension could be found
     */
    protected function getExtension($mime_type)
    {
        $mime = new ApacheMimeTypes\FlatRepository;
        $extension = 'mp4';
        $extensions = $mime->findExtensions($mime_type);
        if (count($extensions)) {
            $extension = $extensions[0];
        }
        return '.' . $extension;
    }
}
