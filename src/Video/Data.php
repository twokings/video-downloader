<?php

namespace Bolt\Extension\Bolt\VideoDownloader\Video;


use Silex\Application;

class Data
{
    private $types;
    private $providers;
    private $app;

    function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Get the overview data and return it as an array
     *
     * @return array
     */
    public function getOverviewData()
    {
        $tg = [];

        // Get all contenttypes that are configured
        $this->getEnabledTypes();
        //$tg['messages'][] = $this->types;
        // Get all providers
        $this->getEnabledProviders();
        //$tg['messages'][] = $this->providers;
        $providers = array_keys($this->providers);

        foreach($this->types as $currenttype) {
            $records['video'] = $this->countRecords($currenttype, '');
            foreach($providers as $provider) {
                $videovalue = $this->providers[$provider]['pattern'];
                $records[$provider] = $this->countRecords($currenttype, $videovalue);
            }
        }

        // Get an overview of all records
        $tg += [
            'video' => [
                'total' => $records['video']['yes'] + $records['video']['no'] + $records['video']['invalid'],
                'processed' => $records['video']['yes'],
                'waiting' => $records['video']['no'],
                'invalid' => $records['video']['invalid']
            ],
            'other' => [
                'total' => $records['video']['other'],
                'processed' => 'n/a',
                'waiting' => 'n/a',
                'invalid' => 'n/a'
            ],
        ];

        // Split up the different types of video
        foreach($this->providers as $key => $provider) {
            $tg += [
                $key => [
                    'total'     => $records[$key]['yes'] + $records[$key]['no'] + $records[$key]['invalid'],
                    'processed' => $records[$key]['yes'],
                    'waiting'   => $records[$key]['no'],
                    'invalid'   => $records[$key]['invalid']
                ]
            ];
        }
        return $tg;
    }

    /**
     * Prepare an array with current active content types from the extension config
     */
    private function getEnabledTypes()
    {
        if(!isset($this->types)) {
            $config = $this->app['videodownloader.config'];
            foreach ($config['import'] as $key => $ctype) {
                if ($ctype['enabled'] == true) {
                    $this->types[$key] = $ctype;
                }
            }
        }
    }

    /**
     * Get an array with current active content types from the extension config
     */
    public function enabledTypes()
    {
        $this->getEnabledTypes();
        return $this->types;
    }

    /**
     * Prepare an array with current active providers from the extension config
     */
    private function getEnabledProviders()
    {
        if(!isset($this->providers)) {
            $config = $this->app['videodownloader.config'];
            foreach ($config['providers'] as $key => $stype) {
                if ($stype['enabled'] == true) {
                    $this->providers[$key] = $stype;
                }
            }
        }
    }

    /**
     * Get an array with current active providers from the extension config
     */
    public function enabledProviders()
    {
        $this->getEnabledProviders();
        return $this->providers;
    }

    /**
     * @param        $currenttype
     * @param string $videovalue
     *
     * @return array
     */
    private function countRecords($currenttype, $videovalue = 'http')
    {
        // quit early if we're no able
        if($currenttype['enabled'] !== true) {
            return [];
        }

        // set variables
        $contenttype = $currenttype['contenttype'];
        $field = $currenttype['field'];
        $statusfield = $currenttype['statusfield'];
        $video = '%'. $videovalue .'%';
        // make sure the output is well formed
        $output = [
            'yes' => 0,
            'no' => 0,
            'invalid' => 0,
            'other' => 0
        ];

        // match contenttype from Bolt with import from extension config
        $ctype = $this->app['config']->get('contenttypes/' . $contenttype);
        // build a simple query to match the different states
        $query = $this->app['db']->createQueryBuilder();
        $records = $query->select('count(id) as total, ' . $statusfield)
            ->from($this->app['schema.prefix'] . $ctype['tablename'])
            //->where($query->expr()->like('videolink', ':video'))
            ->where($field . ' LIKE :video')
            ->orderBy('id')
            ->setParameter('video', $video)
            ->groupBy($statusfield)
            ->execute()
            ->fetchAll()
        ;

        // change the output format for easy handling later on
        foreach($records as $record) {
            $output[$record[$statusfield]] = $record['total'];
        }
        return $output;
    }

    /**
     * Get a list of video's form a given content type
     *
     * @return array
     */
    public function getBatch($contenttypeinput)
    {
        $this->getEnabledTypes();
        $enabledtypes = array_keys($this->types);

        if(in_array($contenttypeinput, $enabledtypes)) {
            // set variables
            $config = $this->app['videodownloader.config'];
            $limit = $config['defaults']['batchlimit'];

            $currenttype = $this->types[$contenttypeinput];
            $contenttype = $currenttype['contenttype'];
            $field = $currenttype['field'];
            $statusfield = $currenttype['statusfield'];

            // match contenttype from Bolt with import from extension config
            $ctype = $this->app['config']->get('contenttypes/' . $contenttype);
            // build a simple query to match the different states
            $query = $this->app['db']->createQueryBuilder();
            $records = $query->select('*')
                ->from($this->app['schema.prefix'] . $ctype['tablename'])
                ->where($statusfield . ' = :status')
                ->setParameter(':status', 'no')
                ->orderBy('id')
                ->setMaxResults($limit)
                ->execute()
                ->fetchAll()
            ;

            //return [
            //    ['type' => 'youtube', 'id' => 'MbxVNC_Amag'],
            //    ['type' => 'vimeo', 'id' => '128691690']
            //];

            $output = [];
            // change the output format for easy handling later on
            foreach($records as $key => $record) {
                $output[$key] = $record;
                //dump($record);
                $out_url = $this->parseVideoUrl($record[$field]);
                //print_r($out_url);
                if($out_url != false) {
                    $output[$key]['type'] = $out_url['type'];
                    $output[$key]['video_id'] = $out_url['video_id'];
                } else {
                    unset($output[$key]);
                    // set status of the record to invalid
                    $this->setDownloadFailed($contenttype, $record['id']);
                }
            }
            return $output;
        }

        return false;
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function setDownloadCompleted($contenttypeinput, $id, $location = 'unknown')
    {
        $this->getEnabledTypes();
        $enabledtypes = array_keys($this->types);

        if(in_array($contenttypeinput, $enabledtypes)) {
            // match contenttype from Bolt with import from extension config
            $currenttype = $this->types[$contenttypeinput];
            $contenttype = $currenttype['contenttype'];
            $statusfield = $currenttype['statusfield'];
            if($currenttype['statusmessage']) {
                $statusmessage = $currenttype['statusmessage'];
            }

            // match contenttype from Bolt with import from extension config
            $ctype = $this->app['config']->get('contenttypes/' . $contenttype);

            $query = $this->app['db']->createQueryBuilder();
            $query->update($this->app['schema.prefix'] . $ctype['tablename'])
                ->set($statusfield, ':valid')
                ->where('id = :id')
                ->setParameter(':valid', 'yes')
                ->setParameter(':id', $id);

            if($currenttype['statusmessage']) {
                $query->set($currenttype['statusmessage'], ':message')
                      ->setParameter(':message', $location);
            }

            $result = $query->execute();
            return $result;
        }

        return false;
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function setDownloadFailed($contenttypeinput, $id)
    {
        $this->getEnabledTypes();
        $enabledtypes = array_keys($this->types);

        if(in_array($contenttypeinput, $enabledtypes)) {
            // match contenttype from Bolt with import from extension config
            $currenttype = $this->types[$contenttypeinput];
            $contenttype = $currenttype['contenttype'];
            $statusfield = $currenttype['statusfield'];

            // match contenttype from Bolt with import from extension config
            $ctype = $this->app['config']->get('contenttypes/' . $contenttype);

            $query = $this->app['db']->createQueryBuilder();
            $result = $query->update($this->app['schema.prefix'] . $ctype['tablename'])
                ->set($statusfield, ':invalid')
                ->where('id = :id')
                ->setParameter(':invalid', 'invalid')
                ->setParameter(':id', $id)
                ->execute()
            ;
            return $result;
        }

        return false;
    }

    /**
     * Checks if a url has a vimeo or youtube video id
     *
     * @param $in_url
     *
     * @return array
     */
    public function parseVideoUrl($in_url)
    {
        $vimeoregexp = '/^http(s)?\:\/\/((player|www)\.)?vimeo\.com\/video\/(.*)$/';

        if($vimeomatched = preg_match($vimeoregexp, $in_url, $matches)) {
            //dump($matches);
            return ['type' => 'vimeo', 'video_id' => $matches[4] ];
        }

        $vimeodirectregexp = '/^http(s)?\:\/\/((player|www)\.)?vimeo\.com\/(.*)$/';

        if($vimeomatched = preg_match($vimeodirectregexp, $in_url, $matches)) {
            //dump($matches);
            return ['type' => 'vimeo', 'video_id' => $matches[4] ];
        }

        $youtuberegexp = '/^http(s)?\:\/\/(www\.)?youtube\.com\/embed\/(.*)$/';
        if($youtubematched = preg_match($youtuberegexp, $in_url, $matches)) {
            //dump($matches);
            return ['type' => 'youtube', 'video_id' => $matches[3] ];
        }

        $youtubedirectregexp = '/^http(s)?\:\/\/(www\.)?youtube\.com\/watch\?v=(.*)$/';
        if($youtubematched = preg_match($youtubedirectregexp, $in_url, $matches)) {
            //dump($matches);
            return ['type' => 'youtube', 'video_id' => $matches[3] ];
        }

        $youtubelinkregexp = '/^http(s)?\:\/\/youtu\.be\/(.*)$/';
        if($youtubematched = preg_match($youtubelinkregexp, $in_url, $matches)) {
            //dump($matches);
            return ['type' => 'youtube', 'video_id' => $matches[2] ];
        }

        return false;
    }
}
