Video downloader for Youtube and Vimeo
======================================

This tool downloads the best quality video source from either Youtube or Vimeo if you give it a video id.

## Installation

Use the bolt extension installer to get this extension. 
After that modify the file `app/config/extensions/videodownloader.bolt.yml`
with your content type and the valid expected video providers.

## Commandline usage

On the commandline run `php app/nut videodownloader:download [--summary] [contenttype]`

- `--summary` shows a summary after the import. Optional.
- `contenttype` for a contenttype that is already in the configuration. 
  Optional, without a given contenttype the extension will use the first enabled contenttype

## Known issues

It will break as soon as Youtube or Vimeo change their datastructure or url's.
The script times out when called via a webserver - but the video will still be downloaded.
Only Vimeo and Youtube are supported.